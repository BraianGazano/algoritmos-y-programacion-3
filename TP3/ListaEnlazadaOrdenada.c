#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct estructuraNodo
{
    int valor;
    struct estructuraNodo *proximo;
} Nodo;

int obtenerLargo(Nodo *lista)
{

    int tamanio = 0;
    if (lista != NULL)
    {
        Nodo *cursor = lista;

        while (cursor->proximo != NULL)
        {
            cursor = cursor->proximo;
            tamanio++;
        }
        tamanio++;
    }
    return tamanio;
}

void imprimirLista(Nodo *lista)
{

    Nodo *cursor = lista;
    if (lista != NULL)
    {
        while (cursor != NULL)
        {
            printf("%d \n", cursor->valor);
            cursor = cursor->proximo;
        }
    }
}

Nodo *eliminarElementoEnPosicion(Nodo *lista, int posicion)
{
    Nodo *cursor = lista;
    if (lista != NULL)
    {

        for (int i = 0; i < posicion - 1; i++)
        {
            cursor = cursor->proximo;
        }
        Nodo *temporal = cursor->proximo;
        cursor->proximo = cursor->proximo->proximo;
        free(temporal);
        temporal = NULL;
    }
    return lista;
}

Nodo *obtenerElementoEnPosicion(Nodo *lista, int posicion)
{
    Nodo *cursor = lista;
    if (lista != NULL)
    {
        for (int i = 0; i < posicion; i++)
        {
            cursor = cursor->proximo;
        }

        return cursor;
    }

    return NULL;
}

void *intercambiarNodos(Nodo *actual, Nodo *siguiente)
{
    int auxiliar = actual->valor;
    actual->valor = siguiente->valor;
    siguiente->valor = auxiliar;
}

Nodo *ordenarLista(Nodo *lista)
{
    bool intercambiado;
    Nodo *cursor;
    if (lista != NULL)
    {
        do
        {
            intercambiado = false;
            cursor = lista;
            while (cursor->proximo != NULL)
            {
                if (cursor->valor > cursor->proximo->valor)
                {
                    intercambiarNodos(cursor, cursor->proximo);
                    intercambiado = true;
                }
                cursor = cursor->proximo;
            }
        } while (intercambiado);
    }
    return lista;
}

Nodo *agregarElemento(Nodo *lista, int valor)
{
    Nodo *nodoNuevo = malloc(sizeof(Nodo));
    nodoNuevo->valor = valor;
    nodoNuevo->proximo = NULL;

    if (lista == NULL)
    {
        lista = nodoNuevo;
    }
    else
    {
        Nodo *cursor = lista;
        while (cursor->proximo != NULL)
        {
            cursor = cursor->proximo;
        }

        cursor->proximo = nodoNuevo;
        lista = ordenarLista(lista);
    }
    return lista;
}

Nodo *crearLista()
{
    Nodo *lista = malloc(sizeof(Nodo));
    lista = NULL;
    return lista;
}

int main()
{
    Nodo *lista = crearLista();
    lista = agregarElemento(lista, 4);
    lista = agregarElemento(lista, 1);
    lista = agregarElemento(lista, 2);
    lista = agregarElemento(lista, 6);
    lista = agregarElemento(lista, 4);
    lista = agregarElemento(lista, 5);
    lista = agregarElemento(lista, 6);
    lista = agregarElemento(lista, 7);
    lista = agregarElemento(lista, 2);
    lista = eliminarElementoEnPosicion(lista, 4);
    Nodo *nodo = obtenerElementoEnPosicion(lista, 3);
    int tamanio = obtenerLargo(lista);
    imprimirLista(lista);
    printf("El tamanio es:  %i\n", tamanio);
    printf("El valor en la posicion es: %d\n", nodo->valor);

    return 0;
}

