#include <stdio.h>

//A partir de un listado de números, determinar el máximo

int main(int argc, char const *argv[])
{
    int numeros[13] = {1, 2, 6, 5, 3, 8, 6, 33, 7, 9, 10, 4, 14};
    int max = numeros[0];
    for (int i = 0; i < 13; i++)
    {
        if (numeros[i] > max)
        {
            max = numeros[i];
        }
    }
    printf("El m%cximo es: %i", 160, max);
    return 0;
}
