#include <stdio.h>

//Diseñar un menú navegable donde cada opción muestre una frase
//distinta. Debe retornar al menú a menos que se elija la opción “Salir”.
//1. Opción 1
//2. Opción 2
//3. Opción 3
//4. Salir
//Ingrese opción:

int main(int argc, char const *argv[])
{
    int opcion;

    while (opcion != 4)
    {

        printf("1. Opcion 1\n2. Opcion 2\n3. Opcion 3\n4. Salir\nElija una opci%cn: ", 162);
        scanf("%i", &opcion);

        if (opcion == 1)
        {
            printf("Soy la opci%cn 1\n", 162);
        }
        else if (opcion == 2)
        {
            printf("Soy la opci%cn 2\n", 162);
        }
        else if (opcion == 3)
        {
            printf("Soy la opci%cn 3\n", 162);
        }
        else if (opcion == 4)
        {
            printf("\nSaliendo del programa");
        }
        else
        {
            printf("Opci%cn incorrecta\n", 162);
        }
    }

    return 0;
}
