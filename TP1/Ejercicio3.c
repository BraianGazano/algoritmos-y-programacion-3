#include <stdio.h>

//A partir de un listado de números, determinar el mínimo

int main(int argc, char const *argv[])
{
    int numeros[13] = {1, 2, 6, 5, 3, 8, 6, -5, 7, 9, 10, -15, 14};
    int minimo = numeros[0];
    for (int i = 0; i < 13; i++)
    {
        if (numeros[i] < minimo)
        {
            minimo = numeros[i];
        }
    }
    printf("El m%cnimo es: %i", 161, minimo);
    return 0;
}
