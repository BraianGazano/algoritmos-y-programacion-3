#include <stdio.h>

//A partir del ingreso de tres enteros determinar el promedio.

int main(int argc, char const *argv[])
{
    int valores[3];

    printf("Ingrese un valor A: ");
    scanf("%i", &valores[0]);
    printf("Ingrese un valor B: ");
    scanf("%i", &valores[1]);
    printf("Ingrese un valor C: ");
    scanf("%i", &valores[2]);

    printf("\nEl promedio es : %.2f", ((valores[0] + valores[1] + valores[2]) * 1.0) / 3);

    return 0;
}
