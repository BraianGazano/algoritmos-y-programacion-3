#include <stdio.h>

//Imprimir todos los caracteres ASCII

int main(int argc, char const *argv[])
{
    printf("C%cdigo\t -\tCar%ccter\n", 162, 160);
    for (int i = 0; i < 256; i++)
    {
        printf("%d\t -\t %c\n", i, i);
    }
    return 0;
}
