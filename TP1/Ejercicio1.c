#include <stdio.h>
//Programar una rutina que reciba nombre y edad de una persona y las
//imprima por pantalla.
//Su nombre es: ######
//Su edad es: #

int main(int argc, char const *argv[])
{
    char nombre[100];
    int edad;

    printf("Ingrese su nombre: ");
    scanf("%s", &nombre);
    printf("Ingrese su edad: ");
    scanf("%i", &edad);

    printf("\nSu nombre es: %s \n", nombre);
    printf("Su edad es: %i", edad);
    return 0;
}
