//Diseñe una estructura que represente una persona que puede tener hasta 2 hijos.
//Con las herramientas vistas hasta ahora ¿qué problemática se presenta en términos de manejo de memoria
#include <stdio.h>

typedef struct
{
    char *nombre;
    char *apellido;
    int edad;
    int dni;
} Padre;
typedef struct
{
    Padre padre;
} Hijo;

int main(int argc, char const *argv[])
{
    Padre padre = {"Juan", "Perez", 41243454, 45};
    Hijo hijoUno = {.padre.nombre = "Alan", .padre.apellido = "Perez", .padre.dni = 42131332, .padre.edad = 10};
    Hijo hijoDos = {.padre.nombre = "Pedro", .padre.apellido = "Perez", .padre.dni = 42135552, .padre.edad = 16};
    printf("Nombre: %s\nApellido: %s\nEdad: %i\nDNI: %i\n\n", hijoUno.padre.nombre, hijoUno.padre.apellido, hijoUno.padre.edad, hijoUno.padre.dni);
    printf("Nombre: %s\nApellido: %s\nEdad: %i\nDNI: %i\n\n", hijoDos.padre.nombre, hijoDos.padre.apellido, hijoDos.padre.edad, hijoDos.padre.dni);

    return 0;
}
