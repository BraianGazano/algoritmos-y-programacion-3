#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//intente modularizar la inicialización de la estructura usando funciones.
//¿Qué dificultades o cambios se presentan? ¿A qué cree que se deben?

typedef struct
{
    char *nombre;
    char *apellido;
    int edad;
    int dni;
} Persona;

void asignarDatos(Persona *persona, char nombre[], char apellido[], int edad, int dni);

int main(int argc, char const *argv[])
{
    Persona *persona = malloc(sizeof(Persona));
    asignarDatos(persona, "Adrian", "Perez", 22, 41233456);
    printf("Nombre: %s\nApellido: %s\nEdad: %i\nDNI: %i", persona->nombre, persona->apellido, persona->edad, persona->dni);

    return 0;
}
void asignarDatos(Persona *persona, char nombre[], char apellido[], int edad, int dni)
{
    persona->edad = edad;
    persona->dni = dni;
    persona->nombre = strdup(nombre);
    persona->apellido = strdup(apellido);
}
