# Preguntas

### 2): ¿Qué dificultades o cambios se presentan? ¿A qué cree que se deben?

En el punto 2 tuve complicaciones al momento de asignarle valores a las variables de la estructura, investigando en internet pude obtener ciertas pistas del porque el codigo no compilaba como es debido y todo se debe a las referencias a memoria, lo cual se pudo solucionar utilizando punteros como se vió en la clase.

### 3)Con las herramientas vistas hasta ahora ¿qué problemática se presenta en términos de manejo de memoria

En el punto 3 tenemos la dificultad de que en C no existe la herencia como tal, por lo que la forma de simular este comportamiento es mediante composicion de estructuras. Lo malo de esto es que mientras mas estructuras anidadas tengamos peor sera el manejo de memoria ya que tendremos que reservar mas cantidad de memoria por cada estructura hija que agreguemos.
