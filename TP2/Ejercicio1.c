#include <stdio.h>
//: Defina una estructura que represente una entidad en la vida real.
//Instancie la estructura en el main, popule los datos e imprímalos.

typedef struct
{
    char nombre[30];
    char apellido[30];
    int edad;
    int dni;
} Persona;

int main(int argc, char const *argv[])
{
    Persona persona = {
        .nombre = "Adrian",
        .apellido = "Perez",
        .dni = 41243454,
        .edad = 22};

    printf("Nombre: %s\nApellido: %s\nEdad: %i\nDNI: %i", persona.nombre, persona.apellido, persona.edad, persona.dni);

    return 0;
}

